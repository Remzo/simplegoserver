package main

import (
	"flag" // for command line flags
	"net/http"
	"strconv"
	
	"math/rand"
	"time"

	"github.com/Sirupsen/logrus"

	"github.com/googollee/go-socket.io"

	"github.com/kardianos/service"
)

var log = logrus.New()

//map of items by connectionID
var hostedItems = make(map[string]string)
//map of sockets by connectionID
var senderSockets = make(map[string]socketio.Socket)

type program struct{}

func (p *program) Start(s service.Service) error {
	if service.Interactive() {
		log.Infoln("Running in terminal.")
	} else {
		log.Infoln("Running under service manager.")
	}
	
	// Start should not block. Do the actual work async.
	go p.run()
	return nil
}

func (p *program) run() {
	
	//random seed
	rand.Seed(time.Now().UnixNano())

	// Do work here
	// server listening port
	listenPortPtr := flag.Int("listenPort", 8787, "Listening port")

	// parse command line flags and dereference the variables
	flag.Parse()
	var listenPort = *listenPortPtr
		
	// initialize server
	server, err := socketio.NewServer(nil)
	if err != nil {
		log.Fatal(err)
	}

	// handle incoming connection
	server.On("connection", func(so socketio.Socket) {
		
		var connectionID = ""
		var isHost = false
		log.Infoln("ClientConnected")
		
		//handle item sender socket
		so.On("Host", func(itemID string) {
			isHost = true
			
			//generate connection ID
			connectionID = RandStringRunes(6)
			log.Infoln("ConnectionID sent: "+connectionID)
			log.Infoln("ItemID received: " + itemID)
			
			//register itemID to connectionID
			hostedItems[connectionID] = itemID
			
			//register sender socket to connectionID
			senderSockets[connectionID] = so

			//send connectionID to sender
			so.Emit("ConnectionID", connectionID)
		})
		
		so.On("ItemReceived", func(connectionID string) {
			log.Infoln("Item received for connection: "+connectionID)
			
			//tell host item was received
			senderSocket, exists := senderSockets[connectionID]
			if exists == true {
				senderSocket.Emit("ItemReceived", "Yes")
			}
			
			//remove hosted items
			DeleteHostedItem(connectionID)
			
			//disconnect receiver
			so.Emit("disconnect")
		})
		
		//handle item receiver
		so.On("GetItem", func(connectionID string) {
			//check that connection id exists
			itemID, exists := hostedItems[connectionID]
			if exists == true {
				//send item ID to receiver
				so.Emit("ItemID", itemID)
				log.Infoln("Sending item for connection "+connectionID + ": " + itemID)
			}else {
				//send invalid connection ID code to receiver
				so.Emit("ItemID", "INVALID")
				log.Infoln("No item for connection: "+connectionID)
			}
		})
		
		so.On("disconnection", func() {
			log.Infoln("ClientDisconnected")
			if isHost == true {
			//remove hosted item if host disconnects
			DeleteHostedItem(connectionID)
			}
		})

	})

	// ## listen for errors
	server.On("error", func(so socketio.Socket, err error) {
		log.Errorln("error:", err)
	})

	// #### start listening to incoming
	http.Handle("/socket.io/", server)
	http.Handle("/", http.FileServer(http.Dir("./asset")))

	// log
	log.Infoln("Server started listening on port:"+strconv.Itoa(listenPort))

	log.Fatal(http.ListenAndServe(":"+strconv.Itoa(listenPort), nil))
}

func (p *program) Stop(s service.Service) error {
	// Stop should not block. Return with a few seconds.
	return nil
}

func main() {
	// setup the logger
	// log = logrus.New()
	// hook, err := logrus_syslog.NewSyslogHook("", "", syslog.LOG_INFO, "")

	// if err == nil {
		// log.Hooks.Add(hook)
	// }

	svcConfig := &service.Config{
		Name:        "simplegoserver service",
		DisplayName: "Backend service for SimpleGoServer!",
		Description: "Backend service for SimpleGoServer!",
	}

	prg := &program{}
	s, err := service.New(prg, svcConfig)
	if err != nil {
		log.Fatal(err)
	}
	// logger, err = s.Logger(nil)
	if err != nil {
		log.Fatal(err)
	}
	err = s.Run()
	if err != nil {
		log.Error(err)
	}
}

func DeleteHostedItem(connectionID string) {
	_, exists := hostedItems[connectionID]
	
	if exists == true {
		delete(hostedItems, connectionID)		
		delete(senderSockets, connectionID)
		log.Infoln("Connection removed: " + connectionID)
	}
}

var letterRunes = []rune("0123456789ABCDEF")

func RandStringRunes(n int) string {
    b := make([]rune, n)
    for i := range b {
        b[i] = letterRunes[rand.Intn(len(letterRunes))]
    }
    return string(b)
}